class OrderDetailsController < ApplicationController
  before_action :set_order_detail, only: [:show, :update, :destroy]

  # GET /order_details
  def index
    @order_details = OrderDetail.all

    render json: @order_details
  end

  # GET /order_details/1
  def show
    render json: @order_detail
  end

  # POST /order_details
  def create
    @order_detail = OrderDetail.new(order_detail_params)

    if @order_detail.save
      render json: @order_detail, status: :created, location: @order_detail
    else
      render json: @order_detail.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /order_details/1
  def update
    if @order_detail.update(order_detail_params)
      render json: @order_detail
    else
      render json: @order_detail.errors, status: :unprocessable_entity
    end
  end

  # DELETE /order_details/1
  def destroy
    @order_detail.destroy
  end

  def getSold #Agrupa operaciones de Analisis

    #Invocacion de todas las funciones

    @prod=OrderDetail.getSoldProducts(params[:id])
    @punt=OrderDetail.getScores(params[:id])
    @timeD = Order.timeDifference(params[:id])
    @statusOrder = Order.porcRebound(params[:id])
    @ordrs = Order.prodSold(params[:id])


    if (@prod != -1)
    
      render json: ["Numero de Pedidos",@ordrs,"Promedio de Productos por Pedido",@prod,"Puntaje Obtenido",@punt,"Frecuencia de Venta en dias",@timeD,"Porcentaje de Rebote",@statusOrder]
    else
      render json: ["Error:", "Datos no Encontrados"]
    
    end
  end



  private
    # Use callbacks to share common setup or constraints between actions.
    def set_order_detail
      @order_detail = OrderDetail.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def order_detail_params
      params.fetch(:order_detail, {})
    end
end
