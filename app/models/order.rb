class Order < ActiveResource::Base
  self.site = "https://pseesapipedidos.herokuapp.com" #coneccion a la API de pedidos



  def self.timeDifference(id) #calcula la diferencia de fechas entre pedidos

    if @details = OrderDetail.find(:all, :from => "/order_details/product_delivered/"+id.to_s)

      @counter = @details.count
  
      @dif = 0
      @sum = 0

      @top = @details.first #obtencion de los detalles de pedidos
      @topOrder = @top.order_id.to_s
      

      if @initial = Order.find(@topOrder)  #para obtener los datos del primer pedido en base al detalle anterior

        	@in = @initial.created_at.to_datetime
        	@keyIn = @initial.id
        	
      end

      @details.each_with_index do |detail,i|
        @dates = @details[i].order_id.to_s  #Iteracion de los detalles de pedidos

        if @ordrs=Order.find(@dates) #Obtencion de los pedidos para compararlos con el base
         
            @times = @ordrs.created_at.to_datetime
            @key = @ordrs.id
            

            if @keyIn != @key #Comparacion de Ids
            
            	@dif = @times - @in
            	
            	@aprox = @dif.to_i
            	@in = @times
            	@sum = @sum + @aprox
            	
            end

        end

      end
     
      if @counter > 0
        @result = @sum
      else
        @result = -1
      end
      return @result.to_i
    end
    

  end

  def self.prodSold(id) #Calcula el numero de pedidos realizados segun el producto

  	if @details = OrderDetail.find(:all, :from => "/order_details/product/" + id.to_s)
  		@det = OrderDetail.find(:all, :from => "/order_details/product/" + id.to_s)
  		@tot = @details.count
  		
  		@ordens = 0
  		@aux = 0
  		

  		@details.each_with_index do |detail,i|

  			@comp = @details[i].order_id
  	
  			@value = i + 1
  			@val = @tot - 1

  			for a in @value..@val do #Evaluando si hay pedidos repetidos

  				if @comp == @det[a].order_id
  					@aux = @aux + 1		
  				end

  			end

  			if @aux > 0
  				@ordens = @ordens + 1
  			end
  			@aux = 0
  			
  		end
  	end
  	return (@tot-@ordens)
  end

  
  def self.porcRebound(id) #Calcula el porcentaje de Rebote del producto

  	
  	if @details = OrderDetail.find(:all, :from => "/order_details/product/" + id.to_s)

      @counter = @details.count
      
      @sum = 0
      

      @details.each_with_index do |detail,i|
        @dates = @details[i].order_id.to_s

        if @ordrs=Order.find(@dates)
         
            @stat = @ordrs.status.to_s #Obtiene el estado del producto
            
            if @stat == "Expirado" or @stat=="Denegado"
            	@sum = @sum + 1
            end

        end

      end

      if @counter > 0

        @result = ((@sum.to_f/@counter.to_f)*100).round(2)	
      else
        @result = -1
      end
      return @result 
    end

  end


end
